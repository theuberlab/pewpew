package lug

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/toolkits/file"
)

var (
	LugLogLevel		=	ERROR
	logger				log.Logger

	config				LugConfig

	// Lug can't use Lug to log messages otherwise the caller referencing creates an infite loop
	logError func(keyvals ...interface{}) error
	logWarn  func(keyvals ...interface{}) error
	logInfo  func(keyvals ...interface{}) error
	logDebug func(keyvals ...interface{}) error

	logUTC				bool
	loglevel			string
	logFormat			string
)

func init() {
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))

	//TODO: Pretty sure this log level thing is broken
	// Set the log level
	switch LugLogLevel {
	case NONE:
		logger = level.NewFilter(logger, level.AllowNone())
	case ERROR:
		logger = level.NewFilter(logger, level.AllowError())
	case WARN:
		logger = level.NewFilter(logger, level.AllowWarn())
	case INFO:
		logger = level.NewFilter(logger, level.AllowInfo())
	case DEBUG:
		logger = level.NewFilter(logger, level.AllowDebug())
	case TRACE:
		logger = level.NewFilter(logger, level.AllowAll())
	}

	logTimeFormat := log.DefaultTimestamp

	if logUTC {
		logTimeFormat = log.DefaultTimestampUTC
	}

	// Set some default values which will appear in all log messages such as the timestamp. Caller lvl 4 shows whatever
	// called one of the below created functions. I.E. where in main we called alog.logError.
	logger = log.With(logger, "ts", logTimeFormat, "caller", "lug internal")

	// Lug can't use Lug to log messages otherwise the caller referencing creates an infite loop
	// Create some aliases for log levels
	logError	= level.Error(logger).Log
	logWarn		= level.Warn(logger).Log
	logInfo		= level.Info(logger).Log
	logDebug	= level.Debug(logger).Log
}

// Get the default (Applied to everything for whidch there isn't a more specific level set) log level
func GetDefaultLevel() LogLevel{
	return config.Default.Level
}

// Get the log format
func GetLogFmt() string {
	return config.Default.Format
}

// Are we logging in UTC?
func GetLogUTC() bool {
	return config.Default.LogUTC
}

// Actually log a message
func logMessage(caller string, level LogLevel, keyvals ...interface{}) error {
	var logvals []interface{}

	logvals = append(logvals, "Level", level.String())

	logvals = append(logvals, "Caller", caller)

	logvals = append(logvals, keyvals...)

	return logger.Log(logvals...)
}

//Logs at the Fatal level
func Fatal(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, FATAL) {
		return nil
	}

	logMessage(caller, FATAL, keyvals...)

	os.Exit(1)
	return nil
}

//Logs at the Error level
func Error(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, ERROR) {
		return nil
	}

	return logMessage(caller, ERROR, keyvals...)
}

//Logs at the Warn level
func Warn(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, WARN) {
		return nil
	}

	return logMessage(caller, WARN, keyvals...)
}

//Logs at the Info level
func Info(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, INFO) {
		return nil
	}

	return logMessage(caller, INFO, keyvals...)
}

//Logs at the Debug level
func Debug(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, DEBUG) {
		return nil
	}

	return logMessage(caller, DEBUG, keyvals...)
}

//Logs at the Trace level
func Trace(keyvals ...interface{}) error {
	caller := getCaller()

	if !doesSourcefileSupportLevel(caller, DEBUG) {
		return nil
	}

	return logMessage(caller, DEBUG, keyvals...)
}

// Returns the name of the file which logged a message
func getCaller() string {
	_, file, _, ok := runtime.Caller(2)

	if ok {
		logDebug("Message", "Determining caller", "Caller", file)
	} else {
		logDebug("Message", "Determining caller", "Caller", "Unable to determine caller")
	}

	return file
}

// Check to see if the caller is configured for the specified level
func doesSourcefileSupportLevel(caller string, logLevel LogLevel) (bool) {

	for _, fileConfig := range config.Files {
		logDebug("Message", "Checking configured filename against caller", "Filename", fileConfig.Filename, "ConfiguredLevel", fileConfig.Level, "Caller", caller)
		if strings.HasSuffix(caller, fileConfig.Filename) {
			// We do have a specific entry for this file. Now lets see if it supports the specified level.
			logDebug("Message", "Found setting for caller")
			if fileConfig.Level >= logLevel {
				logDebug("Message", "Caller does support level", "LogLevel", logLevel, "SupportedLevel", fileConfig.Level, "Caller", caller)
				return true
			} else {
				logDebug("Message", "Caller DOES NOT support level", "LogLevel", logLevel, "Caller", caller, "ConfiguredLevel", fileConfig.Level)
				// We have a specific config but it does not include this level so return false.
				return false
			}

		}
	}

	logDebug("Message", "Caller: DOES NOT have a specific config. Falling back to default:", "OrigCaller", caller)

	// If we are here we did not find a specific entry so use the default config
	if config.Default.Level >= logLevel {
		logDebug("Message", "Default supports level:", "LogLevel", logLevel)
		return true
	} else {
		logDebug("Message", "Default DOES NOT support level", "LogLevel", logLevel)
		return false
	}
}

func InitDefaultLogger() {

	config = LugConfig{
		Default: struct {
			Format			string `json:"format"`
			LevelString		string `json:"level"`
			Level			LogLevel
			LogUTC			bool `json:"logutc"`
			DefaultLabels	[]string		`json:"defaultlabels"`
		}{Format: "logfmt", LevelString: "Warn", Level: WARN, LogUTC: false, DefaultLabels: nil},
	}

	initLogger()
}


// Loads the yaml config file and initializes the logger. Eventually we should parse this into json and pass it up.
func InitLoggerFromYaml(filename string) {
	//log.Close()
	var (
		content string
		err		error
	)

	content, err = readFile(filename)
	if err != nil {
		logError("Message", "InitLoggerFromYaml: Could not read file. Initializing default logger.", "Filename", filename, "Error", err)
		InitDefaultLogger()
		return
	}

	// Let's first convert it to JSON since that's what yaml.Unmarshal does anyway.
	yamlBytes, err := yaml.YAMLToJSON([]byte(content))
	if err != nil {
		logError("Message", "InitLoggerFromYaml: Could not convert from yaml to json. Initializing default logger.", "Filename", filename, "Error", err)
		InitDefaultLogger()
		return
	}

	err = json.Unmarshal(yamlBytes, &config)

	logDebug("Message", "Parsed Yaml for JSON", "JSON", yamlBytes)
	//err = yaml.Unmarshal([]byte(content), config)
	if err != nil {
		logError("Message", "InitLoggerFromYaml: Could not parse yaml configuration file. Initializing default logger.", "Filename", filename, "Error", err)
		InitDefaultLogger()
		return
	}

	//TODO: Write a custom un-marshalar which understands LogLevel and works properly.
	// Set the log level of default and each file from the config string

	config.Default.Level = DetermineLogLevel(config.Default.LevelString)

	for idx, item := range config.Files {
		determinedLevel := DetermineLogLevel(item.LevelString)
		logDebug("Message", "Setting log level for file", "Filename", item.Filename, "LogLevel", determinedLevel)
		config.Files[idx].Level = determinedLevel
	}

	//initLogger(config.Default.Format, config.Default.Level, config.Default.LogUTC)
	initLogger()
}

// Load a configuration file which is in json format (NOT IMPLEMENTED).
func LoadJsonConfiguration(filename string) {
	//log.Close()
	dst := new(bytes.Buffer)
	var (
		config  LugConfig
		content string
	)
	err := json.Compact(dst, []byte(filename))

	if err != nil {
		content, err = readFile(filename)
		if err != nil {
			logDebug("Message", "LoadJsonConfiguration: Error: Could not read file", "Filename", filename, "Error", err)
			os.Exit(1)
		}
	} else {
		content = string(dst.Bytes())
	}

	err = json.Unmarshal([]byte(content), &config)
	if err != nil {
		logDebug("Message", "LoadJsonConfiguration: Error: Could not parse json configuration file", "Filename", filename, "Error", err)
		os.Exit(1)
	}

}

// Pull the file from the disk
func readFile(fileName string) (string, error) {
	// This should only be called if we have been passed a file name. Die.
	if fileName == "" {
		return "", fmt.Errorf("No file provided to readFile")
		//os.Exit(1)
	}

	// Check to see that the file exists.
	if !file.IsExist(fileName) {
		return "", fmt.Errorf("config file %s does not exist", fileName)
		//os.Exit(1)
	}

	content, err := file.ToTrimString(fileName)
	if err != nil {
		return "", fmt.Errorf("Error reading file: %s Error: %v", fileName, err)
	}

	return content, nil
}

// Initialize the logging library
func initLogger() {
	// If the default format is empty set it to logfmt.
	if config.Default.Format == "" {
		config.Default.Format = "logfmt"
	}

	// Create an instance of log with the specified format.
	switch config.Default.Format {
	case "logfmt":
		logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	case "json":
		logger = log.NewJSONLogger(log.NewSyncWriter(os.Stderr))
	}

	//TODO: I think there is a better check to use here.
	// If the default log level is unset set it to Error. Only use None if explicitly set to that.
	if config.Default.Level <= NONE || config.Default.Level >= TRACE {
		logError("Message", "Unknown Default Log Level", "Level", config.Default.Level)
		config.Default.Level = ERROR
	}

	LugLogLevel = config.Default.Level

	logTimeFormat := log.DefaultTimestamp

	if logUTC {
		logTimeFormat = log.DefaultTimestampUTC
	}

	//TODO: Need to change log.caller if we're going to wrap it like this
	// Set some default values which will appear in all log messages such as the timestamp. Caller lvl 4 shows whatever
	// called one of the below created functions. I.E. where in main we called alog.logError.
	logger = log.With(logger, "ts", logTimeFormat)


	logDebug("Message", "Initialized lug", "LogLevel", config.Default.Level, "LogFormat", config.Default.Format, "LogUTC", config.Default.LogUTC)
}
