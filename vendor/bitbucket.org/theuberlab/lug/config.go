package lug

import "strings"

// LugConfig represents json log config struct
type LugConfig struct {
	Default struct {
		Format			string `json:"format"`
		LevelString		string `json:"level"`
		Level			LogLevel
		LogUTC			bool `json:"logutc"`
		DefaultLabels	[]string		`json:"defaultlabels"`
	} `json:"default"`
	Files []struct {
		LevelString		string `json:"level"`
		Level			LogLevel
		Filename		string `json:"name"`
	} `json:"files"`
}


// Create the LogLevel enum
type LogLevel int

const (
	NONE	LogLevel	= iota
	FATAL
	ERROR
	WARN
	INFO
	DEBUG
	TRACE
)

// Gets the string representation of a LogLevel
func (l *LogLevel) String() string {
	names := [...]string{
		"NONE",
		"FATAL",
		"ERROR",
		"WARN",
		"INFO",
		"DEBUG",
		"TRACE",
	}

	if *l < NONE || *l > TRACE {
		return "NONE"
	}

	return names[*l]
}

//TODO: Normalize the input string so that we don't have to be so picky about what folks type
//Turn a string into an enum
func DetermineLogLevel(levelString string) LogLevel {
	enums := make(map[string]LogLevel)

	enums["NONE"]	= NONE
	enums["FATAL"]	= FATAL
	enums["ERROR"]	= ERROR
	enums["WARN"]	= WARN
	enums["INFO"]	= INFO
	enums["DEBUG"]	= DEBUG
	enums["TRACE"]	= TRACE

	logDebug("Message", "normalizing level string", "String", levelString)
	levelString = strings.ToUpper(levelString)

	logDebug("Message", "Normalized", "String", levelString, "EnumValue", enums[levelString])


	return enums[levelString]
}
