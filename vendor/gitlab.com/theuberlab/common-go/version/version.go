package version

import (
	"fmt"
)

type VersionInfo struct {
	Major		int
	Minor		int
	Patch		int
	Build		string
}

var Version = VersionInfo {
	Major:		0,
	Minor:		1,
	Patch:		2,
}

// Returns the version in string format.
func (v *VersionInfo) GetVersionString() string {
	var verstring string

	if v.Build != "" {
		verstring = fmt.Sprintf("%d.%d.%d-%s", v.Major, v.Minor, v.Patch, v.Build)
	} else {
		verstring = fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)
	}

	return verstring
}

// Return a reference to the version object.
func GetVersion() *VersionInfo {
	return &Version
}

// Sets the build portion of the version.
func (v *VersionInfo) SetBuild(build string) *VersionInfo {
	v.Build = build
	return &Version
}