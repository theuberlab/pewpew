# Design

Now that we have working example code we really should start with some more formal design.



# Questions
  * Do we need to start a container at application startup or just when we start the thing which will be making the
  requests?
  * Each http.Client has a connection pool setting. In fact we have to disable it to get the actual concurrency we want.
  So instead of creating 10 runShooter threads for a concurrency of 10 and thus possibly 10 clients. But instead a single
  client with 10 connections.


# App Flow Thoughs
  1) Start app
     1) Validate CL/Config options
     1) Create engagement
     1) Create Req details target object
     1) Create battalion
         1) Create client (shooter) object(s)
         1) Create gunny shooter manager object
        1) Create GIO (openmentrics) collectors
  1) Start shooter manager run
    1) Start timer (move this to the beginning so that we get a better ability to report on the actual software run?
    Actually we might already get this stat from prometheus)
    1) Trigger shooters. Each shooter
       1) Make request
       1) Recieve result
       1) Parse httptrace metrics
       



# TODOS

  * Figure out how to use openmetrics histograms properly I think those are what will give us the p numbers we want
  to report on.
  * Figure out how to generate buckets. I suspect we need to define those either when we create the collector or when
  we report the metric. I _thing_ there's a thing where I can simply record the amount of time it took for each request,
  then define what the buckets should be and have it calculate those when we report the results.
  * There is already an openmetrics collector which works with httptrace. We can probably get
  rid of the results object entirely. The bit that does what we want here: 
  https://github.com/prometheus/client_golang/blob/master/prometheus/promhttp/instrument_client.go. The example code
  here: https://godoc.org/github.com/prometheus/client_golang/prometheus/promhttp#InstrumentRoundTripperDuration


# Objects

  * Shooter - The thing responsible for firing reqests. This might just be the http.Client.
  * GIO - The thing that collects metrics from the Shooters. 
  ([From Ground Intelligence Officer or MOS 0202](https://en.wikipedia.org/wiki/Ground_Intelligence_Officer)
  * MDR - A collection of Shooters. 
  ([From Marine Division Recon](https://en.wikipedia.org/wiki/United_States_Marine_Corps_Reconnaissance_Battalions))
  * FRO - The thing that reports to the GIO? (From Field Radio Operator, a member of an MDR Battalion)
  * Gunny - The thing that manages Shooters and the GIO. (From 
  [Gunnery Sergeant](https://en.wikipedia.org/wiki/Gunnery_sergeant) otherwise known as a "gunny")