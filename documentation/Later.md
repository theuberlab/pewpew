# Functionality I May Want to Support Later

  * tlsKeyLogWriter - Set KeyLogWriter in the tls.Config. From the crypto.tls docs "KeyLogWriter optionally specifies a destination for TLS master secrets in NSS key log format that can be used to allow external programs such as Wireshark to decrypt TLS connections. See https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Key_Log_Format . Use of KeyLogWriter compromises security and should only be used for debugging"
  * Rate limit by requests
  * Rate limit each transfer
  * Number of CPUs to use?
  * -B ipv4-addr <address>      Address to bind to when making outgoing connections

# Some Options From AB Which Are Interesting
    -B address      Address to bind to when making outgoing connections

    -b windowsize   Size of TCP send/receive buffer, in bytes
    -disable-compression  Disable compression.
      -disable-keepalive    Disable keep-alive, prevents re-use of TCP
                            connections between different HTTP requests.
      -disable-redirects    Disable following of HTTP redirects
    
# Some Options From Curl Which Are Interesting

-#, --progress-bar
              Make curl display transfer progress as a simple progress bar instead of the standard, more informational, meter.

              This progress bar draws a single line of '#' characters across the screen and shows a percentage if the transfer size
              is known. For transfers without a known size, it will instead output one '#' character for every  1024  bytes  trans-
              ferred.

-K, --config <file> if I want to support this

      --local-port <num/range>
              Set  a preferred single number or range (FROM-TO) of local port numbers to use for the connection(s).  Note that port
              numbers by nature are a scarce resource that will be busy at times so setting this  range  to  something  too  narrow
              might cause unnecessary connection setup failures.

              Added in 7.15.2.
              
              
--connect-to <HOST1:PORT1:HOST2:PORT2>

      --local-port <num/range>
              Set  a preferred single number or range (FROM-TO) of local port numbers to use for the connection(s).  Note that port
              numbers by nature are a scarce resource that will be busy at times so setting this  range  to  something  too  narrow
              might cause unnecessary connection setup failures.

              Added in 7.15.2.

  -w, --write-out <format> A way to customize what the report writes

       -v, --verbose
              Makes curl verbose during the operation. Useful for debugging and seeing what's going on "under  the  hood".  A  line
              starting with '>' means "header data" sent by curl, '<' means "header data" received by curl that is hidden in normal
              cases, and a line starting with '*' means additional info provided by curl.

              If you only want HTTP headers in the output, -i, --include might be the option you're looking for.

       -u, --user <user:password>
              Specify the user name and password to use for server authentication. Overrides -n, --netrc and --netrc-optional.

              If you simply specify the user name, curl will prompt for a password.



 -D, --dump-header <filename> writes headers to the specified file

-c, --cookie-jar <filename> Write cookies to the specified file

       -b, --cookie <data> reads cookies from the specified file.


       -S, --show-error
              When used with -s, --silent, it makes curl show an error message if it fails.

       --trace-time
              Prepends a time stamp to each trace or verbose line that curl displays.

              Added in 7.14.0.

       --trace <file>
              Enables  a  full trace dump of all incoming and outgoing data, including descriptive information, to the given output
              file. Use "-" as filename to have the output sent to stdout. Use "%" as filename to have the output sent to stderr.

              If this option is used several times, the last one will be used.

              This option overrides -v, --verbose and --trace-ascii.


-i, --include
              Include the HTTP-header in the output. The HTTP-header includes things like server-name, date of the document,  HTTP-
              version and more...

              See also -v, --verbose.

       -k, --insecure
       --keepalive-time <seconds>
              This option sets the time a connection needs to remain idle before sending keepalive  probes  and  the  time  between
              individual  keepalive  probes. It is currently effective on operating systems offering the TCP_KEEPIDLE and TCP_KEEP-
              INTVL socket options (meaning Linux, recent AIX, HP-UX and more). This option has  no  effect  if  --no-keepalive  is
              used.

              If this option is used several times, the last one will be used. If unspecified, the option defaults to 60 seconds.

              Added in 7.18.0.
       -L, --location
              (HTTP)  If  the  server reports that the requested page has moved to a different location (indicated with a Location:
              header and a 3XX response code), this option will make curl redo the request on the new place. If used together  with
              -i,  --include  or -I, --head, headers from all requested pages will be shown. When authentication is used, curl only
              sends its credentials to the initial host. If a redirect takes curl to a different host, it won't be able  to  inter-
              cept  the  user+password. See also --location-trusted on how to change this. You can limit the amount of redirects to
              follow by using the --max-redirs option.

              When curl follows a redirect and the request is not a plain GET (for example POST or PUT), it will do  the  following
              request  with a GET if the HTTP response was 301, 302, or 303. If the response code was any other 3xx code, curl will
              re-send the following request using the same unmodified method.

              You can tell curl to not change the non-GET request method to GET after a 30x response by using the dedicated options
              for that: --post301, --post302 and --post303.
Do this when we add the above
      --max-redirs <num>
              (HTTP)  Set  maximum  number  of redirection-followings allowed. When -L, --location is used, is used to prevent curl
              from following redirections "in absurdum". By default, the limit is set to 50 redirections. Set this option to -1  to
              make it unlimited.

              If this option is used several times, the last one will be used.

-x, --proxy [protocol://]host[:port]
              Use the specified proxy.

              The proxy string can be specified with a protocol:// prefix. No protocol specified or http:// will be treated as HTTP
              proxy.  Use socks4://, socks4a://, socks5:// or socks5h:// to request a specific SOCKS version to be used.  (The pro-
              tocol support was added in curl 7.21.7)


This is an interesting option from the curl man page
       -M, --manual
              Manual. Display the huge help text.

       -r, --range <range>
              (HTTP FTP SFTP FILE) Retrieve a byte range (i.e a partial document) from a HTTP/1.1, FTP or SFTP server  or  a  local
              FILE. Ranges can be specified in a number of ways.

              0-499     specifies the first 500 bytes

              500-999   specifies the second 500 bytes

              -500      specifies the last 500 bytes

              9500-     specifies the bytes from offset 9500 and forward

              0-0,-1    specifies the first and last byte only(*)(HTTP)

              100-199,500-599
                        specifies two separate 100-byte ranges(*) (HTTP)

              (*) = NOTE that this will cause the server to reply with a multipart response!

Would be nice to be able to specify this but have it only do a partial download then send a RST to the server.
Have a way to do that OR use the range option.
       --max-filesize <bytes>
              Specify  the  maximum  size  (in  bytes)  of a file to download. If the file requested is larger than this value, the
              transfer will not start and curl will return with exit code 63.

              NOTE: The file size is not always known prior to download, and for such files this option has no effect even  if  the
              file transfer ends up being larger than this given limit. This concerns both FTP and HTTP transfers.

              See also --limit-rate.

       -E, --cert <certificate[:password]>
              (TLS)  Tells  curl  to use the specified client certificate file when getting a file with HTTPS, FTPS or another SSL-
              based protocol. The certificate must be in PKCS#12 format if using Secure Transport, or PEM format if using any other
              engine.   If  the  optional  password  isn't specified, it will be queried for on the terminal. Note that this option
              assumes a "certificate" file that is the private key and the client certificate  concatenated!  See  -E,  --cert  and
              --key to specify them independently.

       --ciphers <list of ciphers>
              (TLS)  Specifies  which  ciphers to use in the connection. The list of ciphers must specify valid ciphers. Read up on
              SSL cipher list details on this URL:

               https://curl.haxx.se/docs/ssl-ciphers.html

              If this option is used several times, the last one will be used.

       --compressed
              (HTTP) Request a compressed response using one of the algorithms curl supports, and save the  uncompressed  document.
              If this option is used and the server sends an unsupported encoding, curl will report an error.

 -F, --form <name=content>
              (HTTP)  This  lets  curl  emulate a filled-in form in which a user has pressed the submit button. This causes curl to
              POST data using the Content-Type multipart/form-data according to RFC 2388. This enables uploading  of  binary  files
              etc. To force the 'content' part to be a file, prefix the file name with an @ sign. To just get the content part from
              a file, prefix the file name with the symbol <. The difference between @ and < is  then  that  @  makes  a  file  get
              attached  in  the post as a file upload, while the < makes a text field and just get the contents for that text field
              from a file.

-G, --get Useful to force a get with a body when -d triggers a post.

       -I, --head
              (HTTP FTP FILE) Fetch the headers only! HTTP-servers feature the command HEAD which this uses to get nothing but  the
              header  of a document. When used on an FTP or FILE file, curl displays the file size and last modification time only.
		Actually causes curl to make a HEAD request.

       -0, --http1.0
              (HTTP) Tells curl to use HTTP version 1.0 instead of using its internally preferred HTTP version.

              This option overrides --http1.1 and --http2.

       --http1.1
              (HTTP) Tells curl to use HTTP version 1.1.

              This option overrides -0, --http1.0 and --http2. Added in 7.33.0.

       --http2

