# Pew Pew

Pew Pew is a simple HTTP benchmarking tool written in Go. It is inspired by `ab` (Apache Bench) and `curl`.

Pew Pew fires requests at a specified url, traces various components of the request timings and reports the results by 
writing a file in openmetrics (prometheus) format.


# Building
Currently build with go
```go build -o pew```


# Usage
To run pew pew with the default values simply call it with a URL as an argument.

```pew https://www.google.com```

To find all arguments Pew Pew supports pass it with the -h or --help flag.

```
$ ./pew -h
usage: pew [<flags>] <URL>

An http benchmarking tool inspired by AB.

Flags:
  -h, --help                 Show context-sensitive help (also try --help-long and --help-man).
  -V, --version              Show application version.
  -n, --num-requests=11      Number of requests to run. Default is 11
  -c, --concurrency=3        Number of connections to open concurrently.
  -m, --max-time=30m0s       Maximum run time for the entire benchmark test.
  -s, --silent               Silent or quiet ode. Causes pewpew to not write to stdout.
  -T, --connect-timeout=30s  Max time to wait for a connection to be established. 0 for inifinite.
  -t, --timeout=30s          Max time to wait for a response after sending a request.
  -X, --request="GET"        Specifies the request method to use when performing requests. The specified method will be used instead
                             of the default.
  -k, --insecure             Ignore certificate errors when making TLS connections.
  -H, --header=HEADER        Specify an arbitrary header to send as part of the request such as "Connection: close"
  -d, --data=DATA            Data to send. If you start the data with the letter @, the rest should be a file name to read the data
                             from. Sets the HTTP method to POST unless -X --request <method> is also specified.

Args:
  <URL>  The url to fire requests at.
```



# Output
Pew pew is intended to be used in automation such as CICD pipelines to evaluate performance over time. Therefore it's
output isn't great for humans.

Pew pew writes it's output in OpenMetrics (prometheus) format. It uses the promtthp libraries InstrumentRoundTripperDuration,
InstrumentRoundTripperTrace, and InstrumentRoundTripperCounter functions to instrument the requests.

The resulting output values are a float64 representing the time, in seconds, since the start of the http request.

For example in the following output
```
# HELP pew_pew_setup_tls_duration_seconds Connection (TLS) latencies.
# TYPE pew_pew_setup_tls_duration_seconds summary
pew_pew_setup_tls_duration_seconds{event="tls_handshake_done",quantile="0.1"} 0.699962227
pew_pew_setup_tls_duration_seconds{event="tls_handshake_done",quantile="0.2"} 0.701417865
pew_pew_setup_tls_duration_seconds{event="tls_handshake_done",quantile="0.5"} 1.019097826
pew_pew_setup_tls_duration_seconds{event="tls_handshake_done",quantile="0.9"} 1.04504512
pew_pew_setup_tls_duration_seconds{event="tls_handshake_done",quantile="0.99"} 1.08023387
pew_pew_setup_tls_duration_seconds_sum{event="tls_handshake_done"} 9.982202508
pew_pew_setup_tls_duration_seconds_count{event="tls_handshake_done"} 11
pew_pew_setup_tls_duration_seconds{event="tls_handshake_start",quantile="0.1"} 0.561829651
pew_pew_setup_tls_duration_seconds{event="tls_handshake_start",quantile="0.2"} 0.574028058
pew_pew_setup_tls_duration_seconds{event="tls_handshake_start",quantile="0.5"} 0.892188223
pew_pew_setup_tls_duration_seconds{event="tls_handshake_start",quantile="0.9"} 0.90607205
pew_pew_setup_tls_duration_seconds{event="tls_handshake_start",quantile="0.99"} 0.946935326
pew_pew_setup_tls_duration_seconds_sum{event="tls_handshake_start"} 8.5442762
pew_pew_setup_tls_duration_seconds_count{event="tls_handshake_start"} 11
```

The quantile="0.5" (or 50%) for handshake_start is 0.892188223
The quantile="0.5" (or 50%) for handshake_end is 1.019097826

Which means that 50% of our requests took under 0.126909603 seconds (1.019097826 - 0.892188223) to complete.


# Known issues
  * There is no way to redirect output. It is simply written to Results.out
  * Some flags have not been tested well if at all.
  * No attempt has yet been made for error handling and logging is currently crap so there are cases where a request
    failure will simply result in unexpectedly short results. If your output doesn't contain values for tls_handshake_done
    think_start or think_end you probably need to add -k
