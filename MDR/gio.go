package MDR

import (
	"io"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

//TODO: Take objectiveTargets as an arg
var (
	metricsNamespace			= "pew_pew"
	objectiveTargets			= map[float64]float64{0.1: 0.05, 0.2: 0.05, 0.5: 0.05, 0.9: 0.05, 0.99: 0.05 }
)

//[From Ground Intelligence Officer or MOS 0202](https://en.wikipedia.org/wiki/Ground_Intelligence_Officer)

// The GIO is the bit that actually takes the intelligence (results reported to the FRO.) and turns them in to
// a report. Which for now is just prometheus.WriteToTextfile("<stats file>", prometheus.DefaultGatherer)
// but we might want to support custom output formats/reports in the future.
type GIO struct {
	// See ExampleInstrumentRoundTripperDuration. The example link under
	// here https://godoc.org/github.com/prometheus/client_golang/prometheus/promhttp#InstrumentRoundTripperDuration
	// That might be where I saw and example of someone doing some printing of a string.
	// Go over the methods below that as well (some of which are included in that example.) InstrumentRoundTripperInFlight
	// and InstrumentRoundTripperTrace
	// Where to write the report. // BufferedWriter maybe? Might need to be just a string if I'm going to use WriteToTextfile.
	ReportFile 			io.Writer

	//TODO: Replace this with the above.
	ReportFileName		string

	// Counter for attempted requests
	AttReqs						prometheus.Counter
	// Counter for failed requests
	FailedReqs					prometheus.Counter
}

// Returns a newly minted GIO object.
func CreateGIO() (*GIO, error) {
	//outfile, err := os.Create("results.out")
	//if (err != nil) {
	//	return &GIO{}, err
	//}

	//defer outfile.Close()
	//
	//writer := bufio.NewWriter(outfile)

	officer := GIO{ReportFileName: "Results.out"}

	return &officer, nil
}




// builds the http.Client (Weapon) to use.
func (g *GIO) MakeClient(transport *http.Transport, timeout time.Duration) *http.Client {
	// First make the metrics

	g.AttReqs = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"request",
			Name:		"attempted_requests_total",
			Help:		"Total number of requests attempted for this metrics run.",
		},
	)

	g.FailedReqs = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"request",
			Name:		"failed_requests_total",
			Help:		"Total number of failed requests for this metrics run.",
		},
	)

	sucReqCounter := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"request",
			Name:		"successful_requests_total",
			Help:		"Total number of successful requests performed for this metrics run.",
		},
		[]string{},
	)

	dnsLatencyVec := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"setup",
			Name:		"dns_duration_seconds",
			Help:		"DNS latencies.",
			Objectives:	objectiveTargets, //which will compute 50th percentile with error window of 0.05.
		},
		[]string{"event"},
	)

	connLatencyVec := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"setup",
			Name:		"conn_duration_seconds",
			Help:		"Connection (TCP) latencies.",
			Objectives:	objectiveTargets, //which will compute 50th percentile with error window of 0.05.
		},
		[]string{"event"},
	)

	tlsLatencyVec := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"setup",
			Name:		"tls_duration_seconds",
			Help:		"Connection (TLS) latencies.",
			Objectives:	objectiveTargets, //which will compute 50th percentile with error window of 0.05.
		},
		[]string{"event"},
	)

	respLatencyVec := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"transaction",
			Name:		"req_duration_seconds",
			Help:		"Response latencies.",
			Objectives:	objectiveTargets, //which will compute 50th percentile with error window of 0.05.
		},
		[]string{"event"},
	)

	transactionVec := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:	metricsNamespace,
			Subsystem:	"transaction",
			Name:		"transaction_duration_seconds",
			Help:		"Total transaction (req + resp) Latencies.",
			Objectives:	objectiveTargets, //which will compute 50th percentile with error window of 0.05.
		},
		[]string{"code", "method"},
	)

	// Register said metrics
	prometheus.MustRegister(g.AttReqs, g.FailedReqs, sucReqCounter, dnsLatencyVec, connLatencyVec, tlsLatencyVec, respLatencyVec, transactionVec)

	//TODO: Figure out the difference between GotConn and ConnectDone
	reqTrace := &promhttp.InstrumentTrace{
		DNSStart: func(t float64) {
			dnsLatencyVec.WithLabelValues("dns_start").Observe(t)
		},
		DNSDone: func(t float64) {
			dnsLatencyVec.WithLabelValues("dns_done").Observe(t)
		},
		ConnectStart: func(t float64) {
			connLatencyVec.WithLabelValues("conn_start").Observe(t)
		},
		ConnectDone: func(t float64) {
			connLatencyVec.WithLabelValues("conn_done").Observe(t)
		},
		TLSHandshakeStart: func(t float64) {
			tlsLatencyVec.WithLabelValues("tls_handshake_start").Observe(t)
		},
		TLSHandshakeDone: func(t float64) {
			tlsLatencyVec.WithLabelValues("tls_handshake_done").Observe(t)
		},
		WroteRequest: func(t float64) {
			respLatencyVec.WithLabelValues("think_start").Observe(t)
		},
		GotFirstResponseByte: func(t float64) {
			respLatencyVec.WithLabelValues("think_done").Observe(t)
		},
	}

	durationRTTracer := promhttp.InstrumentRoundTripperDuration(transactionVec, transport)

	reqTracer := promhttp.InstrumentRoundTripperTrace(reqTrace, durationRTTracer)

	transp := promhttp.InstrumentRoundTripperCounter(sucReqCounter, reqTracer)

	//TODO: Add CheckRedirect: checkRedirectFunc
	client := &http.Client{
		Transport:	transp,
		Timeout: 	timeout,
		//CheckRedirect: checkRedirectFunction // Add when we want to customize things like max redirects or not to follow them at all.
		//Jar: CookieJar // Specify the cookie jar to use to know which cookies to send. I don't understand this arg though.
	}

	return client
}

// Writes out the metris report
func (g *GIO) Report() {
	// Write out the results
	prometheus.WriteToTextfile(g.ReportFileName, prometheus.DefaultGatherer)

}
