package MDR

import (
	"fmt"
	"net/http"
	"strings"
)

// Disposition is an http headers object
type Disposition http.Header

func (h *Disposition) Set(value string) error {
	parts := strings.SplitN(value, ":", 2)
	if len(parts) != 2 {
		return fmt.Errorf("expected HEADER:VALUE got '%s'", value)
	}

	// Trim off any leading or trailing whitespace.
	parts[1] = strings.TrimSpace(parts[1])

	(*http.Header)(h).Add(parts[0], parts[1])
	return nil
}

func (h *Disposition) String() string {
	return ""
}
