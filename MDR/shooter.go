package MDR

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type Shooter struct {
	// The request to make
	Ordenance		*http.Request

	// How many requests to make
	AmtOrdenance	int

	// A reference to the client to use when making the request
	Weapon			*http.Client

	// The offset from application start when this Shooter began it's engagement.
	runStart		time.Duration

	// A reference to the engagement.
	Engagement		*Engagement
}

// Starts a shooter which will perform the specified number of requests using the specified client.
func (s *Shooter) Engage(offset time.Duration, stopChan *chan struct{}) {
	s.runStart = offset

	for j := 0; j < s.AmtOrdenance; j++ {
		// Check if application is stopped. Do not send into a closed channel.
		select {
		case <-*stopChan:
			return
		default:
			s.fireRequest()
		}
	}
}

// Make the actual http request
func (s *Shooter) fireRequest() {
	//var respSize int64
	//var respCode int

	s.Engagement.AttReqs.Inc()

	//TODO: Evaluate response code and response size.

	resp, err := s.Weapon.Do(s.Ordenance)

	if err != nil {
		if (s.Engagement.Silent != true) {
			fmt.Printf("Error making request. Error [%s]\n", err.Error())
		}

		s.Engagement.FailedReqs.Inc()
	} else {
		//respSize = resp.ContentLength
		//respCode = resp.StatusCode
		if (s.Engagement.Silent != true) {
			fmt.Printf("No error making request.\n")
		}
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}
}
