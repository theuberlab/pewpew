package MDR

import (
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

var (
	defaultExpenditureTarget	 		= 11 // Until we pass initial testing then it should be more like 200.
	// How many shooters taking shots concurrently
	defaultNumShooters					= 3 // Until we pass initial testing, then it should be 10 or 20.
	defaultSilent 						= false
	// Maximum amount of time to allow the entire benchmark to run.
	defaultTourLength					= 30 * time.Minute
)

// An Engagement is the collection of "orders". I.E. the settings for the benchmarking run.
type Engagement struct {
	// How many shots to fire all told
	ExpenditureTarget	 		int
	// How many shooters taking shots concurrently
	NumShooters					int
	//TODO: Look at how curl does this. Silent is used to prevent it from writing it's normal messages. However if you tell it to return a report with say -w that will go to stdout if you don't specifically send it to a file.
	// Report to STDOUT or not.
	Silent 						bool
	// Maximum amount of time to allow the entire benchmark to run.
	TourLength					time.Duration
	// Where to fire our requests at.
	Target						*Target
	// Has this Engagement been initialized and is ready to be used.
	isInitialized 				bool
	// Holds the time the application started
	appStart					time.Time
	// Counter for attempted requests
	AttReqs						prometheus.Counter
	// Counter for failed requests
	FailedReqs					prometheus.Counter
}

func NewDefaultEngagement(target *Target) *Engagement {
	return &Engagement{
		ExpenditureTarget:	defaultExpenditureTarget,
		NumShooters:		defaultNumShooters,
		Silent:				defaultSilent,
		TourLength:			defaultTourLength,
		Target:				target,
		isInitialized:		false,
	}
}

// Returns a time.Duration since application start
func (e *Engagement) TimeSinceStart() time.Duration {
	return time.Since(e.appStart)
}
