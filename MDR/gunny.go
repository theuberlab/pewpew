package MDR

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

// From [Gunnery Sergeant](https://en.wikipedia.org/wiki/Gunnery_sergeant) otherwise known as a "gunny"

// Manages NumShooters
type Gunny struct {
	// Possibly []http.Weapon? Or even just http.Weapon? We should make this a single http.Weapon with the
	//maxConnections set to concurrency. If someone wants multiple workers each with a connection pool they probably
	//want a different tool which you can run multiple instances of on different nodes.)
	Battalion		*Battalion

	Engagement		*Engagement
	//FRO and FRO?

	// A reference to the http.Weapon
	Client 			*http.Client

	// Determines when to stop the run
	stopChannel		chan struct{}

	// Holds the time.Duration of the start of the run
	runStart		time.Duration

	// Used to ensure init only runs once.
	onceHandle		sync.Once
}



// Builds the http.Request to use
func createRequest(method string, targetURL string) (*http.Request, error) {

	req, err := http.NewRequest(method, targetURL, nil)
	if (err != nil) {
		return &http.Request{}, err
	}

	return req, nil
}

// Returns a clone of the provided *http.Request.
func cloneRequest(r *http.Request, body *[]byte) *http.Request {
	// Create a shallow copy of the struct
	newReq := new(http.Request)
	*newReq = *r

	// Copy all Headers
	newReq.Header = make(http.Header, len(r.Header))
	for k, s := range r.Header {
		newReq.Header[k] = append([]string(nil), s...)
	}

	// Copy the request body if it exists.
	if (body != nil) {
		newReq.Body = ioutil.NopCloser(bytes.NewReader(*body))
	}

	return newReq
}


// Creates the TLS Client config and transport for use in the client.
func (g *Gunny) makeTransport() *http.Transport {
	// Then create the client
	tlsClientConfig := tls.Config{
		//ServerName: "example.com", // Override the host to verify.
		InsecureSkipVerify:	g.Engagement.Target.Insecure, // Set to ignore certificate errors.
	}

	tr := &http.Transport{
		TLSClientConfig:		&tlsClientConfig,
		MaxConnsPerHost:		1,
		DisableKeepAlives:		true, //TODO: Turn this into a var.
		ResponseHeaderTimeout:	g.Engagement.Target.RespTimeout,
		//MaxIdleConns:       10, //TODO: Determine the best value to use here.
		//IdleConnTimeout:    30 * time.Second,
		//DisableCompression: true,
	}

	//TODO: Implement an http2 option and do http2.ConfigureTransport(tr)

	tr.TLSNextProto = make(map[string]func(string, *tls.Conn) http.RoundTripper)

	return tr
}

func CreateGunny(engagement *Engagement, battalion *Battalion, appStart time.Time) (*Gunny, error) {
	var shooters []Shooter

	gunny := Gunny{
		Engagement: engagement,
		Battalion:	battalion,
	}

	gunny.runStart = engagement.TimeSinceStart()

	baseReq, err := http.NewRequest(engagement.Target.Method, engagement.Target.URL.String(), nil)
	if (err != nil) {
		return &Gunny{}, err
	}

	if (len(engagement.Target.Headers)) > 0 {
		fmt.Printf("Target has %d additional headers\n", len(engagement.Target.Headers))

		for k, v := range engagement.Target.Headers {
			baseReq.Header.Add(k, v)
		}

	}

	// Initialize the transport and client
	trans := gunny.makeTransport()
	gunny.Client = battalion.GIO.MakeClient(trans, engagement.TourLength)

	engagement.AttReqs = battalion.GIO.AttReqs
	engagement.FailedReqs = battalion.GIO.FailedReqs

	// Calculate the modulus to determine if we have to parse out a couple of extra requests.
	mod := engagement.ExpenditureTarget % engagement.NumShooters
	quot := engagement.ExpenditureTarget / engagement.NumShooters

	for i := 0; i < engagement.NumShooters; i++ {
		req := cloneRequest(baseReq, engagement.Target.Data)
		amtOrd := quot
		if (i < mod) {
			amtOrd = amtOrd + 1
		}
		shooters = append(shooters, Shooter{
			Ordenance:		req,
			AmtOrdenance:	amtOrd,
			Weapon:			gunny.Client,
			Engagement:		engagement,
		})
	}

	gunny.Battalion.Shooters = shooters

	gunny.onceHandle.Do(func() {
		gunny.stopChannel = make(chan struct{}, engagement.NumShooters)
	})

	return &gunny, nil
}


// Sends the stop signal to all shooters
func (g *Gunny) Stop() {
	for i := 0; i < g.Engagement.NumShooters; i++ {
		g.stopChannel <- struct{}{}
	}

	g.Battalion.GIO.Report()
}

// Do the things we have been configured to do.
func (g *Gunny) BeginEngagement() {
	//fmt.Println("Beginning Engagement: Time %s", g.Engagement.appStart.Add(g.runStart))

	var waitGroup sync.WaitGroup

	waitGroup.Add(g.Engagement.NumShooters)

	for i :=0; i < g.Engagement.NumShooters; i++ {
		go func(shooter int) {
			g.Battalion.Shooters[shooter].Engage(g.Engagement.TimeSinceStart(), &g.stopChannel)
			waitGroup.Done()
		}(i)
	}

	waitGroup.Wait()


	g.Battalion.GIO.Report()
}
