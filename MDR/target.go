package MDR

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"

	"gitlab.com/theuberlab/common-go/version"
)

//TODO: Figure out how to use the default from the library.

var (
	defaultConnectTimeout 		= 30 * time.Second
	defaultRespTimeout			= 30 * time.Second
	defaultTotReqMax			= 120 * time.Second
	defaultInsecure 			bool
	defaultMethod				= "GET"
	defaultHeaders 				= map[string]string{"Accept": "*/*"}
)

// A target is a collection of all the request settings. This is the blueprint which the http.request the client,
//transport and the tlsClientConfig are created off of.
type Target struct {
	// Max time to wait for a connection to be established. Includes DNS lookup, tcp connection, and TLS handshake.
	ConnectTimeout 		time.Duration
	// Max time to wait for a response after sending a request.
	RespTimeout 		time.Duration
	// Max time to allow a request to remain active. Includes connection, request send, and response receive.
	TotReqMax 			time.Duration
	// Use insecure TLS
	Insecure 			bool
	// The method to use when making the request
	Method 				string
	// All headers (including default) to send in a request.
	Headers 			map[string]string
	// What it says on the tin
	URL 				*url.URL
	// Data, if any, to send in the body of the request.
	Data 				*[]byte
	// Has this Target been initialized and is ready to be used.
	isInitialized 		bool
}

// Parses the URL for validity and corrects some errors.
func (t *Target) validateAndFixURL() error {

	if (*t.URL == (url.URL{})) {
		return errors.New("URL not provided.")
	}


	if (len(t.URL.Scheme) == 0) {
		fmt.Println("No scheme specified in URL. Assuming https")
		t.URL.Scheme = "https"
	} else if !(strings.HasPrefix(t.URL.Scheme, "http")) {
		return errors.New(fmt.Sprintf("Scheme [%s] is undefined",t.URL.Scheme))
	}

	fmt.Printf("URL has: %s\n", t.URL)
	return nil
}

// Initialize the target.
// Performs some validation of supplied settings
func (t *Target) Initialize() error {
	// Check and update the URL.
	err := t.validateAndFixURL()
	if (err != nil) {
		return err
	}

	t.isInitialized = true
	return nil
}

// Returns a Target with default values
func NewDefaultTarget(version *version.VersionInfo) *Target {
	defaultHeaders["User-Agent"]= fmt.Sprintf("PewPew/%s",version.GetVersionString())

	return &Target{
		ConnectTimeout: defaultConnectTimeout,
		RespTimeout:    defaultRespTimeout,
		TotReqMax:      defaultTotReqMax,
		Insecure:       false,
		Method:         "GET",
		Headers:		defaultHeaders,
		isInitialized:  false,
	}
}



// Reports if the Target object has been fully initialized.
func (t *Target) IsInitialized() bool {
	return t.isInitialized
}


//TODO: Move the header parsing code into here.

//resp= 30 * time.Second
//conn  = 30 * time.Second
// tot req timeout