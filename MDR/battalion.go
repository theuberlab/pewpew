package MDR

import (
	"errors"
	"time"
)

//[From Marine Division Recon](https://en.wikipedia.org/wiki/United_States_Marine_Corps_Reconnaissance_Battalions)

// Responsible for managing targets and engagement against said targets.
type Battalion struct {
	Gunny			*Gunny
	Shooters		[]Shooter
	GIO 			*GIO
	Engagement		*Engagement
}

// Returns an initialized Battalion
func CreateDefaultBattalion(engagement *Engagement, appStart time.Time) (*Battalion, error) {
	if (!engagement.Target.isInitialized) {
		return &Battalion{}, errors.New("URL is undefined")
	}

	battalion := Battalion{
		Engagement:	engagement,
	}
	var err error

	battalion.GIO, err = CreateGIO()
	if (err != nil) {
		return &Battalion{}, err
	}

	battalion.Gunny, err = CreateGunny(engagement, &battalion, appStart)
	if (err != nil) {
		return &Battalion{}, err
	}

	return &battalion, nil
}