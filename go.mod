module gitlab.com/theuberlab/pewpew

go 1.14

require (
	bitbucket.org/theuberlab/lug v0.9.1
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d
	github.com/beorn7/perks v1.0.1
	github.com/cespare/xxhash/v2 v2.1.1
	github.com/ghodss/yaml v1.0.0
	github.com/go-kit/kit v0.10.0
	github.com/go-logfmt/logfmt v0.5.0
	github.com/golang/protobuf v1.4.2
	github.com/matttproud/golang_protobuf_extensions v1.0.1
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.10.0
	github.com/prometheus/procfs v0.1.3
	github.com/toolkits/file v0.0.0-20160325033739-a5b3c5147e07
	gitlab.com/theuberlab/common-go v0.0.0-20200320054107-48fbbab52d43
	golang.org/x/sys v0.0.0-20200728102440-3e129f6d46b1
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.3.0
)
