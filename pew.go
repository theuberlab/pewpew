package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"github.com/alecthomas/kingpin"
	"gitlab.com/theuberlab/common-go/version"
	"gitlab.com/theuberlab/pewpew/MDR"
)

//TODO: Add a pipeline which builds binaries properly and sets the version as a tag.
//TODO: Error out if we cannot even establish a connection, dns failure, etc.

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app				= kingpin.New("pew", "An http benchmarking tool inspired by ab and curl.")
	versionInfo		*version.VersionInfo
	battalion		*MDR.Battalion
	appStart		= time.Now()
)

// Initialize some components that need to be available early on.
func init() {

	//TODO: Get actual version from a git tag at build time.
	versionInfo = version.GetVersion()
	versionInfo.Major = 0
	versionInfo.Minor = 1
	versionInfo.Patch = 3

	appVersion := versionInfo.GetVersionString()

	var data string
	target := MDR.NewDefaultTarget(versionInfo)
	engagement := MDR.NewDefaultEngagement(target)

	headerStrings := map[string]string{}
	// Setup Kingpin flags
	// Set the application version number

	var method string

	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	app.VersionFlag.Short('V')

	app.Flag("num-requests", fmt.Sprintf("Number of requests to run. Default is %d", engagement.ExpenditureTarget)).Short('n').Default(strconv.Itoa(engagement.ExpenditureTarget)).IntVar(&engagement.ExpenditureTarget)

	app.Flag("concurrency", "Number of connections to open concurrently.").Short('c').Default(strconv.Itoa(engagement.NumShooters)).IntVar(&engagement.NumShooters)

	app.Flag("max-time", "Maximum run time for the entire benchmark test.").Short('m').Default(engagement.TourLength.String()).DurationVar(&engagement.TourLength)

	app.Flag("silent", "Silent or quiet ode. Causes pewpew to not write to stdout.").Short('s').Default(strconv.FormatBool(engagement.Silent)).BoolVar(&engagement.Silent)

	app.Flag("connect-timeout", "Max time to wait for a connection to be established. 0 for inifinite.").Short('T').Default(target.ConnectTimeout.String()).DurationVar(&target.ConnectTimeout)

	app.Flag("timeout","Max time to wait for a response after sending a request.").Short('t').Default(target.RespTimeout.String()).DurationVar(&target.RespTimeout)

	app.Flag("request", "Specifies the request method to use when performing requests. The specified method will be used instead of the default.").Short('X').Default("NOT_SUPPLIED").StringVar(&method)

	app.Flag("insecure", "Ignore certificate errors when making TLS connections.").Short('k').Default(strconv.FormatBool(target.Insecure)).BoolVar(&target.Insecure)

	app.Flag("header", "Specify an arbitrary header to send as part of the request such as \"Connection: close\"").Short('H').StringMapVar(&headerStrings)

	//target.Headers = getHeaderObjectFromMap()


	app.Arg("URL", "The url to fire requests at.").Required().URLVar(&target.URL)

	//TODO: Update this to use a special function which either writes the data to the var or parses the file.
	app.Flag("data", "Data to send. If you start the data with the letter @, the rest should be a file name to read the data from. Sets the HTTP method to POST unless -X --request <method> is also specified.").Short('d').StringVar(&data)

	// I probably want to use this
	//app.PreAction()

	//TODO: Implement this. Take a directory as an argument and write an output file per request.
	//app.Flag("output-dir", "The directory in which to write the response results.").Short('o').StringVar(&outDir)

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	for k, v := range headerStrings {
		target.Headers[k] = v
	}

	if !(data == "") {

		if (strings.HasPrefix(data, "@")) {
			fmt.Println("We got a file name, openning it to fetch the data.")
			dataFile := strings.TrimPrefix(data, "@")
			data, err := ioutil.ReadFile(dataFile)
			if (err != nil) {
				app.FatalUsage(fmt.Sprintf("Error reading data from file: %s", data))
			}

			target.Data = &data
		}
	}

	if (method == "NOT_SUPPLIED") {
		if !(data == "") {
			target.Method = "POST"
		} else {
			target.Method = "GET"
		}
	}

	//fmt.Printf("Target method is [%s]\n", target.Method)
	//fmt.Printf("Headers has %v\n", headerStrings)
	//fmt.Printf("Data has: %v", target.Data)


	// Don't bother initializing the battalion if we haven't been provided a URL.
	err := target.Initialize()
	if (err != nil) {
		app.FatalUsage(fmt.Sprintf("Unable to start application target: %s", err.Error()))
	}

	battalion, err = MDR.CreateDefaultBattalion(engagement, appStart)

	if (err != nil) {
		app.FatalUsage(fmt.Sprintf("Unable initialize application battalion: %s", err.Error()))
	}

}

//TODO: Start using this
//
func getHeaderObjectFromMap(addHeaders map[string]string) (headers *http.Header) {
	headers = &http.Header{}

	for k, v := range addHeaders {
		headers.Add(k, strings.TrimSpace(v))
	}
	return headers
}

func main() {
	//TDOO: Move pretty much all of this into battalion

	if (battalion.Engagement.Silent == false) {
		fmt.Println("Application Startup")
	}

	//fmt.Printf("Target has these headers: %s", battalion.Engagement.Target.Headers)
	// Create a stop channel
	c := make(chan os.Signal, 1)
	// Capture ctrl-c and trigger the stop channel if we catch it.
	signal.Notify(c, os.Interrupt)

	//TODO this stop signal catching isn't working. Make it work

	// Stop our shooters if we catch a stop signal.
	go func() {
		<-c
		battalion.Gunny.Stop()
	}()

	// Stop our shooters if we exceed our max run time.
	if battalion.Engagement.TourLength > 0 {
		go func() {
			time.Sleep(battalion.Engagement.TourLength)
			battalion.Gunny.Stop()
		}()
	}

	battalion.Gunny.BeginEngagement()
}